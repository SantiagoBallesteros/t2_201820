package test;


import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.LinkedList;

public class LinkedListTest {

	private LinkedList<Integer> list;
	
	public void setup1(){
		list = new LinkedList<>();
		list.addAtEnd(5);
		list.addAtEnd(7);
		list.addAtEnd(10);
		list.addAtEnd(1);
	}
	@Test
	public void testGetSize(){
		setup1();
		assertEquals("No da el tama�o esperado", 4, list.getSize());
	}
	@Test
	public void testGetElement()
	{
		setup1();
		assertEquals(Integer.valueOf(1), list.getElement(list.getSize()-1));
	}
	@Test
	public void testDeleteElement()
	{
		setup1();
		assertFalse("No debio haber eleminado un elemento", list.removeElement(11));
		assertTrue("No se removio el elemento", list.removeElement(5));
	}
	@Test
	public void testIsEmpty()
	{
		setup1();
		assertFalse("La lista no deberia estar vacia",  list.isEmpty());
	}
	
	@Test
	public void testAddAtk()
	{
		setup1();
		list.addAtK(7, 3);
		assertEquals("El elemento no se a�adio en el lugar indicado", Integer.valueOf(7), list.getElement(3));
	}

}
