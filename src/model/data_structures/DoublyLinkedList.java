package model.data_structures;

import model.data_structures.LinkedList.Node;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {
	
	
	public void addElement(T element);
	
	public void addAtEnd(T element);
	
	public boolean addAtK(T element, int k);
	
	public boolean removeElement(T element);
	
	public Node<T> getNode(int pos);
	
	public T getElement(int pos);
	
	public int getSize();

	public Node<T> getCurrentElement();
	
	public boolean remove(int pos);
	
	
	

	

}
