package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.LinkedList;

public class DivvyTripsManager<T> implements IDivvyTripsManager {

	private LinkedList<VOTrip> trips = new LinkedList<>();
	private LinkedList<VOStation> stations = new LinkedList<>();
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try {
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] line = reader.readNext();
			while((line = reader.readNext()) != null){
				int id = Integer.parseInt(line[0]);
				String name = line[1];
				String city = line[2];
				double latitude = Double.parseDouble(line[3]);
				double longitude = Double.parseDouble(line[4]);
				int capacity = Integer.parseInt(line[5]);
				String date = line[6];
				VOStation station = new VOStation(id, name, city, latitude, longitude, capacity, date);
				stations.addElement(station);
			}
		    reader.close();

			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try {
			int counter = 0;
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] line = reader.readNext();
			while((line = reader.readNext()) != null){
				String user = line[9];
				int birthyear = (line[11].equals(""))? 0 : Integer.parseInt(line[11]);
				if(user.toLowerCase().equals("subscriber")){
					
					
					VOTrip trip = new VOTrip(Integer.parseInt(line[0]), line[1], line[2], Integer.parseInt(line[3]), Integer.parseInt(line[4]),
										Integer.parseInt(line[5]), 
										line[6], Integer.parseInt(line[7]), 
										line[8], line[9], line[10], 
										birthyear);
					trips.addElement(trip);
				}
				else if(user.toLowerCase().equals("customer")){
					
					VOTrip trip = new VOTrip(Integer.parseInt(line[0]), line[1], line[2], Integer.parseInt(line[3]), Integer.parseInt(line[4]),
										Integer.parseInt(line[5]), line[6], Integer.parseInt(line[7]), line[8], line[9]);
					trips.addElement(trip);
				}
			
				
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		LinkedList<VOTrip> copy = new LinkedList<>();
		Iterator<VOTrip> iter = trips.iterator();
		
		while(iter.hasNext()){
			
			VOTrip trip = iter.next();
			if(trip.getGender() != null && trip.getGender().toLowerCase().equals(gender.toLowerCase())){
				copy.addElement(trip);
			}
		}
		
		return copy;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		LinkedList<VOTrip> copy = new LinkedList<>();
		Iterator<VOTrip> iter = trips.iterator();
		while(iter.hasNext()){
			VOTrip trip = iter.next();
			if(trip.getToStationId() == stationID){
				copy.addElement(trip);
			}
		}
		return copy;
		
		
	}	


}
