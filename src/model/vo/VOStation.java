package model.vo;

public class VOStation {
	
	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude, longitude;
	
	private int capacity;
	
	private String date;
	public VOStation(int id, String name, String city, double latitude, double longitude, int capacity, String date) {
		// TODO Auto-generated constructor stub
		this.setId(id);
		this.setName(name);
		this.setCity(city);
		this.setLatitude(latitude);
		this.setLongitude(longitude);
		this.setCapacity(capacity);
		this.setDate(date);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

}
